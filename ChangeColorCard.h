#pragma once
#include "Card.h"
class ChangeColorCard :
	public Card
{
public:
	ChangeColorCard();
	bool isPlayable(Card* another);
};

