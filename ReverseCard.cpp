#include "ReverseCard.h"



ReverseCard::ReverseCard(Colors col)
{
	color = col;
	number = Special::Reverse;
	special = true;
}

bool ReverseCard::isPlayable(Card * another)
{
	if (this->color == another->getColor() ||
		this->number == another->getNumber())
	{
		return true;
	}
	else
	{
		return false;
	}
}

