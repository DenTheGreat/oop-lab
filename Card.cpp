#include "Card.h"
#include <iostream>
#include <vector>
#include <string>
using namespace std;


Card::Card()
{

}

Card::Card(int num, Colors col)
{
	color = col;
	number = num;
	if (num < 0) special = true;
	else special = false;
}

Colors Card::getColor()
{
	return color;
}

int Card::getNumber()
{
	return number;
}

void Card::setColor(Colors k)
{
	color = k;
}

bool Card::isSpecial()
{
	return special;
}


