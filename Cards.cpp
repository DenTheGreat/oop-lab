#include "Cards.h"

Card * CardFactory(int num, Colors col)
{
	Card * result;
	switch (num)
	{
	case Special::Skip:
		result = new SkipCard(col);
		break;
	case Special::Reverse:
		result = new ReverseCard(col);
		break;
	case Special::PlusTwo:
		result = new PlusTwoCard(col);
		break;
	case Special::ChangeColor:
		result = new ChangeColorCard();
		break;
	case Special::PlusFour:
		result = new PlusFourCard();
		break;
	default:
		result = new RegularCard(num, col);
		break;
	}
	return result;
}
