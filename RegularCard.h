#pragma once
#include "Card.h"
class RegularCard :
	public Card
{
public:
	RegularCard(int num, Colors col) : Card(num, col){}
	bool isPlayable(Card* another);
};

