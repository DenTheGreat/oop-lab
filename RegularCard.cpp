#include "RegularCard.h"

bool RegularCard::isPlayable(Card * another)
{
	if (this->color == another->getColor() ||
		this->number == another->getNumber())
	{
		return true;
	}
	else
	{
		return false;
	}
}
