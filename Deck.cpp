#include "Deck.h"
#include <iostream>
#include <vector>
#include <string>
#include <ctime>
using namespace std;



Deck::Deck()
{
	for (size_t j = 0; j < 4; j++)
	{
		for (size_t i = 1; i <= 9; i++)
		{
			Cards.push_back(CardFactory(i, colors[j]));
			Cards.push_back(CardFactory(i, colors[j]));
		}
		Cards.push_back(CardFactory(0, colors[j]));
		Cards.push_back(CardFactory(Special::Skip, colors[j]));
		Cards.push_back(CardFactory(Special::Skip, colors[j]));
		Cards.push_back(CardFactory(Special::Reverse, colors[j]));
		Cards.push_back(CardFactory(Special::Reverse, colors[j]));
		Cards.push_back(CardFactory(Special::PlusTwo, colors[j]));
		Cards.push_back(CardFactory(Special::PlusTwo, colors[j]));
		Cards.push_back(CardFactory(Special::PlusFour,Colors::Black));
		Cards.push_back(CardFactory(Special::ChangeColor, Colors::Black));
	}
}

Card* Deck::draw()
{
	srand(time(0));
	int index = rand() % (Cards.size() - 1);
	Card *result = Cards[index];
	if (index != 0)
	{
		Cards.erase(Cards.begin() + index - 1);
	}
	else
	{
		Cards.erase(Cards.begin());
	}
	return result;
}




