#pragma once
#include <string>
#include <iostream>
#include "Hand.h"
using namespace std;

class Interface
{
public:
	Interface();
	void operator<<(Hand p);
	void operator<<(Card* c);
	void operator<<(string s);
	void operator<<(char s);
	void operator>>(int &a);
	void showPlayable(Hand player, Card* current);
	void turnText(Hand player, int playerNum, Card* currentCard);
	void winMessage(int playerNum);
	short newColor();
	int getNumberOfPlayers();
private:
	void printCard(Card *c);
};

