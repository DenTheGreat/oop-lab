#include "Hand.h"
#include <iostream>
#include <vector>
using namespace std;

Hand::Hand(Deck & d)
{
	for (int i = 0; i < 7; i++)
	{
		draw(d);
	}
}

void Hand::draw(Deck & d)
{
	Cards.push_back(d.draw());
}

int Hand::getFirstPlayable(Card* current)
{
	for (int i = 0; i < Cards.size(); i++)
	{
		if ((*Cards[i]).isPlayable(current))
		{
			return i;
		}
	}
	return -1;
}

Colors Hand::getMaxColor()
{
	int R = 0, Y = 0, G = 0, B = 0;
	for (int i = 0; i < Cards.size(); i++)
	{
		Colors temp = (*Cards[i]).getColor();
		switch (temp)
		{
		case Colors::Red:
			R++;
			break;
		case Colors::Yellow:
			Y++;
			break;
		case Colors::Green:
			G++;
			break;
		case Colors::Blue:
			B++;
			break;
		default:
			break;
		}
	}
	if (R > Y && R > G && R > B)
	{
		return Colors::Red;
	}
	else if (Y > G && Y > B)
	{
		return Colors::Yellow;
	}
	else if (G > B)
	{
		return Colors::Green;
	}
	else
	{
		return Colors::Blue;
	}
}

bool Hand::win()
{
	if (Cards.size() == NULL)
	{
		return true;
	}
	else return false;
}

void Hand::removeCard(int index)
{
	Cards.erase(Cards.begin() + index);
}
