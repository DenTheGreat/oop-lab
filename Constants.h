#pragma once
enum Special
{
	Reverse = -1,
	Skip = -2,
	PlusTwo = -3,
	PlusFour = -5,
	ChangeColor = -6
};

enum class Colors : char
{
	Red = 'r',
	Yellow = 'y',
	Green = 'g',
	Blue = 'b',
	Black = 's'
};

constexpr auto Bot = 1;
const Colors colors[] = { Colors::Red,Colors::Yellow,Colors::Green,Colors::Blue };