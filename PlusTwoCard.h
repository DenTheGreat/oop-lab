#pragma once
#include "Card.h"
class PlusTwoCard :
	public Card
{
public:
	PlusTwoCard(Colors col);
	bool isPlayable(Card* another);
};

