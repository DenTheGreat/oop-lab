#pragma once
#include "Hand.h"
#include "Interface.h"
#include <vector>

class Gameboard
{
public:
	static Gameboard * getInstance() {
		if (!pInstance)
			pInstance = new Gameboard();
		return pInstance;
	}
private:
	static Gameboard * pInstance;
	Gameboard(const Gameboard&);
	Gameboard();
	void game(); // �������� ����
	Interface Console;
	bool botMatch;
	int numberOfPlayers;
	int iterator;
	std::vector<Hand> players;
	Deck deck;
	Card* currentCard;
	int currentPlayer;
	void gameBot(); // ���� ������ ����������
	void drawTwo(int index);
	void play(Card *C); // ������� ����� �� ����
	bool winCheck(int &curPl); //�������� ��������� ����
};