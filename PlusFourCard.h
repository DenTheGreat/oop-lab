#pragma once
#include "Card.h"
class PlusFourCard :
	public Card
{
public:
	PlusFourCard();
	bool isPlayable(Card* another);
};

