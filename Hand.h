#pragma once
#include "PileOfCards.h"
#include "Deck.h"

class Hand :
	public PileOfCards
{
public:
	Hand(Deck &d); // ����������� � ����������� �� ������
	void draw(Deck &d); // ����� ���� �� ������
	int getFirstPlayable(Card* current); // ����������� ����� ��� �����
	Colors getMaxColor(); // ����������� ����� ��� �����
	bool win(); // �������� ������
	void removeCard(int index); // ����������� ����� ��� play ������ GameBoard
};

