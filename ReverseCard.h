#pragma once
#include "Card.h"
class ReverseCard :
	public Card
{
public:
	ReverseCard(Colors col);
	bool isPlayable(Card* another);
};

