#include "SkipCard.h"


SkipCard::SkipCard(Colors col)
{
	color = col;
	number = Special::Skip;
	special = true;
}

bool SkipCard::isPlayable(Card * another)
{
	if (this->color == another->getColor() ||
		this->number == another->getNumber())
	{
		return true;
	}
	else
	{
		return false;
	}
}
