#pragma once
#include "Constants.h"

class Card
{
public:
	Card();
	Card(int num, Colors col); //����������� ��� ������� �������� �����
	virtual Colors getColor(); // ������ ���� �����
	virtual int getNumber();  // ������ ����� �� �����
	virtual void setColor(Colors k); // ���������� ���� �� ����� (��� ������ ����)
	virtual bool isSpecial(); // ������ ���� �� ������ ������ � �����
	virtual bool isPlayable(Card* another) = 0; // ������ ����� �� ������� ��� ����� �� ������
protected:
	Colors color;
	int number;
	bool special;
};