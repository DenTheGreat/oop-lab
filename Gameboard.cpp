#include "Gameboard.h"
#include <vector>
#include <ctime>
#include <Windows.h>
using namespace std;



Gameboard* Gameboard::pInstance = 0;

Gameboard::Gameboard()
{
	numberOfPlayers = Console.getNumberOfPlayers();
	if (numberOfPlayers == 1)
	{
		botMatch = true;
		numberOfPlayers++;
	}
	else
	{
		botMatch = false;
	}
	currentPlayer = 0;
	iterator = 1;
	for (int i = 0; i < numberOfPlayers; i++)
	{
		Hand temp(deck);
		players.push_back(temp);
	}
	currentCard = deck.draw();
	Console << "���� ������� �������.\n";
	game();
}

void Gameboard::game()
{
	if (!botMatch) {
		do {
			if (currentPlayer >= numberOfPlayers ||
				currentPlayer < 0)
			{
				currentPlayer = 0;
			}
			int action;
			Console.turnText(players[currentPlayer], currentPlayer, currentCard);
		in: Console >> action;
			if (action < players[currentPlayer].getSize())
			{
				if ((*players[currentPlayer].Cards[action]).isPlayable(currentCard))
				{
					Card *temp = players[currentPlayer].Cards[action];
					players[currentPlayer].removeCard(action);
					play(temp);
				}
				else
				{
					Console << "����� ������� ������\n������� ���������� ��������: ";
					goto in;
				}
			}
			else players[currentPlayer].draw(deck);
			currentPlayer += iterator;
			system("cls");
			Console << "��� ���������� ������";
			Sleep(1000);
			system("cls");
		} while (!winCheck(currentPlayer));
		Console.winMessage(currentPlayer);
	}
	else gameBot();
}

void Gameboard::gameBot()
{
	int action;
	do {
		if (currentPlayer >= numberOfPlayers ||
			currentPlayer < 0)
		{
			currentPlayer = 0;
		}
		if (currentPlayer != Bot)
		{
			Console.turnText(players[currentPlayer], currentPlayer, currentCard);
			/////////////////////////////////����� ����������������� ����
			Console << "���� ����: ";
			Console	<< "\n";
			Console << players[Bot];
			/////////////////////////////////
		in: Console >> action;
			if (action < players[currentPlayer].getSize())
			{
				if ((*players[currentPlayer].Cards[action]).isPlayable(currentCard))
				{
					Card* temp = players[currentPlayer].Cards[action];
					players[currentPlayer].removeCard(action);
					play(temp);
				}
				else
				{
					Console << "����� ������� ������\n������� ���������� ��������: ";
					goto in;
				}
			}
			else players[currentPlayer].draw(deck);
		}
		else
		{
			action = players[currentPlayer].getFirstPlayable(currentCard);
			if (action == -1)
			{
				players[currentPlayer].draw(deck);
			}
			else
			{
				Card* temp = players[currentPlayer].Cards[action];
				players[currentPlayer].removeCard(action);
				play(temp);
			}
		}
		currentPlayer += iterator;
		system("cls");
	} while (!winCheck(currentPlayer));
	if (currentPlayer == Bot) Console << "������� ���������\n";
	else Console << "������� �����\n";
}

void Gameboard::drawTwo(int index)
{
	players[index].draw(deck);
	players[index].draw(deck);
}

void Gameboard::play(Card *C)
{
	if (!(*C).isSpecial())
	{
		currentCard = C;
	}
	else
	{
		currentCard = C;
		int target = currentPlayer + iterator;
		if (currentPlayer == numberOfPlayers ||
			currentPlayer < 0)
		{
			currentPlayer = 0;
		}
		switch ((*C).getNumber()) // ���������� ������������ ��������
		{
		case Reverse:
			iterator *= -1;
			if (numberOfPlayers == 2)
			{
				currentPlayer++;
			}
			break;
		case Skip:
			currentPlayer++;
			if (currentPlayer == numberOfPlayers)
			{
				currentPlayer = 0;
			}
			break;
		case PlusTwo:
			currentPlayer++;
			if (currentPlayer == numberOfPlayers)
			{
				currentPlayer = 0;
			}
			drawTwo(target);
			break;
		case PlusFour:
			currentPlayer++;
			if (currentPlayer == numberOfPlayers)
			{
				currentPlayer = 0;
			}
			drawTwo(target);
			drawTwo(target);
		case ChangeColor:
			short input;
			if (botMatch && currentPlayer == Bot)
			{
				(*currentCard).setColor(players[currentPlayer].getMaxColor());
			}
			else
			{
			k1: input = Console.newColor();
				if (input > 3 || input < 0)
				{
					Console << "ERROR\n";
					goto k1;
				}
				(*currentCard).setColor(colors[input]);
			}
			break;
		}
	}
}

bool Gameboard::winCheck(int & curPl)
{
	for (int i = 0; i < numberOfPlayers; i++)
	{
		if (players[i].win())
		{
			curPl = i;
			return true;
		}
	}
	return false;
}
