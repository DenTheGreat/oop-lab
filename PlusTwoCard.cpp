#include "PlusTwoCard.h"



PlusTwoCard::PlusTwoCard(Colors col)
{
	color = col;
	number = Special::PlusTwo;
	special = true;
}

bool PlusTwoCard::isPlayable(Card* another)
{
	if (this->color == another->getColor() ||
		this->number == another->getNumber())
	{
		return true;
	}
	else
	{
		return false;
	}
}
