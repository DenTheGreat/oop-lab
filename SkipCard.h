#pragma once
#include "Card.h"
class SkipCard :
	public Card
{
public:
	SkipCard(Colors col);
	bool isPlayable(Card* another);
};

