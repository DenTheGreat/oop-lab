#pragma once
#include "RegularCard.h"
#include "SkipCard.h"
#include "ChangeColorCard.h"
#include "PlusFourCard.h"
#include "ReverseCard.h"
#include "PlusTwoCard.h"

Card* CardFactory(int num, Colors col);