#include "Interface.h"



Interface::Interface()
{
	
}

void Interface::operator<<(Hand p)
{
	for (int i = 0; i < p.getSize(); i++)
	{
		cout << i << ") ";
		printCard(p.Cards[i]);
	}
}

void Interface::operator<<(Card* c)
{
	printCard(c);
}

void Interface::operator<<(string s)
{
	cout << s;
}

void Interface::operator<<(char s)
{
	cout << s;
}

void Interface::operator>>(int & a)
{
	cin >> a;
}

void Interface::showPlayable(Hand player, Card *current)
{
	int counter = 0;
	for (int i = 0; i < player.Cards.size(); i++)
	{
		if ((*player.Cards[i]).isPlayable(current))
		{
			cout << i << ") ";
			*this << player.Cards[i];
			counter++;
		}
	}
	if (counter == NULL)
	{
		cout << "� ��� ��� ����, ������� ������ ����� ������� " << endl;
	}
}

void Interface::turnText(Hand player, int playerNum, Card *currentCard)
{
	cout << "��� ������ " << playerNum + 1 << ':' << endl
		<< "���� ����: " << endl;
	*this << player;
	cout << "������� ����� �� �����:" << endl;
	*this << currentCard;
	cout << "������ �� ������ �������: " << endl;
	showPlayable(player, currentCard);
	cout << "��� " << player.getSize() << ") ����� ����� �� ������" << endl
		<< "�������� ��������: ";
}

void Interface::winMessage(int playerNum)
{
	cout << "������� ����� �" << playerNum + 1 << endl;
}

short Interface::newColor()
{
	short k;
	cout << "�������� ����� ����" << endl
		<< "\t\t0) �������" << endl
		<< "\t\t1) Ƹ����" << endl
		<< "\t\t2) ������" << endl
		<< "\t\t3) �����" << endl;
	cin >> k;
	return k;
}

int Interface::getNumberOfPlayers()
{
	int numberOfPlayers;
	cout << "������� ���������� �������: ";
	cin >> numberOfPlayers;
	return numberOfPlayers;
}

void Interface::printCard(Card *c)
{
	string col;
	switch ((*c).getColor())
	{
	case Colors::Red:
		col = "�������";
		break;
	case Colors::Yellow:
		col = "Ƹ����";
		break;
	case Colors::Blue:
		col = "�����";
		break;
	case Colors::Green:
		col = "������";
		break;
	case Colors::Black:
		col = "����������";
		break;
	}
	if ((*c).isSpecial())
	{
		string effect;
		switch ((*c).getNumber())
		{
		case Reverse:
			effect = "�������� �����������";
			break;
		case PlusTwo:
			effect = "������ 2";
			break;
		case Skip:
			effect = "������� ����";
			break;
		case PlusFour:
			effect = "������ 4";
			break;
		case ChangeColor:
			effect = "�������� ����";
			break;
		}
		cout << '\t' + col + " ����� " + effect + "\n";
	}
	else
	{
		cout << '\t' + col + " ����� " + to_string((*c).getNumber()) + "\n";
	}
}
